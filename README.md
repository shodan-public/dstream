# dstream - Shodan Streaming CLI

## Usage:

```
dstream
Shodan firehose command line interface

USAGE:
    dstream [OPTIONS] <SUBCOMMAND>

OPTIONS:
        --batch-size <BATCH_SIZE>    [default: 5]
    -h, --host <HOST>                [default: https://stream.shodan.io]
        --help                       Print help information
        --lines

SUBCOMMANDS:
    download    Generate data files from Firehose
    help        Print this message or the help of the given subcommand(s)
    publish     Republish data from Firehose to queue services
    stream
```
