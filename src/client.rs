use std::io;

use reqwest::Client;
use tokio::io::AsyncBufReadExt;
use tokio::time::Duration;
use tokio_stream::{wrappers::LinesStream, Stream, StreamExt};
use tokio_util::io::StreamReader;

#[derive(Default)]
enum StreamKind {
    #[default]
    Full,
    Custom(String),
}

#[derive(Default)]
pub struct FirehoseClient {
    client: Client,
    api_key: String,
    base_url: Option<String>,
    lines: bool,
    batch_size: usize,
    stream_kind: StreamKind,
}

impl FirehoseClient {
    pub fn new(api_key: String) -> Self {
        let client = Client::builder()
            .connect_timeout(Duration::from_secs(30))
            .connection_verbose(true)
            .build()
            .unwrap();
        Self {
            client,
            api_key,
            batch_size: 5,
            ..Default::default()
        }
    }

    pub fn base_url(mut self, base_url: String) -> Self {
        self.base_url = Some(base_url);
        self
    }

    pub fn lines(mut self) -> Self {
        self.lines = true;
        self
    }

    pub fn chunks(mut self, batch_size: usize) -> Self {
        self.batch_size = batch_size;
        self
    }

    pub fn full(mut self) -> Self {
        self.stream_kind = StreamKind::Full;
        self
    }

    pub fn custom(mut self, query: impl Into<String>) -> Self {
        self.stream_kind = StreamKind::Custom(query.into());
        self
    }

    pub async fn into_stream(self) -> impl Stream<Item = bytes::Bytes> {
        let base_url = self
            .base_url
            .unwrap_or("https://stream.shodan.io".to_string());
        let stream = self
            .client
            .get(format!("{}/shodan/banners?key={}", base_url, self.api_key))
            .send()
            .await
            .unwrap()
            .bytes_stream()
            .map(|value| value.map_err(|err| io::Error::new(io::ErrorKind::Other, err)));
        let stream = if self.lines {
            let reader = StreamReader::new(stream);
            let stream =
                LinesStream::new(reader.lines()).map(|value| value.map(|banner| banner.into()));
            futures_util::StreamExt::boxed_local(stream)
        } else {
            futures_util::StreamExt::boxed_local(stream)
        };
        let stream = stream.map(|value| value.unwrap());
        futures_util::StreamExt::chunks(stream, self.batch_size)
            .map(|value| value.into_iter().flatten().collect())
    }
}
