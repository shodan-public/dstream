use std::fmt;

use serde::Deserialize;
use tokio_stream::StreamExt;

use crate::client::FirehoseClient;

#[derive(Deserialize)]
struct Banner {
    port: u32,
}

impl fmt::Display for Banner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({})", self.port)
    }
}

pub async fn handler(client: FirehoseClient) {
    let mut stream = client.full().lines().chunks(1).into_stream().await;
    loop {
        let banners = stream.next().await;
        if let Some(banners) = banners {
            let banner = serde_json::from_slice::<Banner>(&banners).unwrap();
            println!("{}", banner);
        }
    }
}
