use std::path::PathBuf;

use chrono::{Local, SecondsFormat};
use tokio::fs::{self, File};
use tokio::io::{AsyncWriteExt, BufWriter};
use tokio::time;
use tokio_stream::StreamExt;

use super::publish::PubsubService;
use crate::client::FirehoseClient;
use crate::Context;

/// Generate data files from Firehose
#[derive(Clone, Debug, clap::Args)]
pub struct Args {
    /// Download directory
    #[clap(short = 'd')]
    directory: PathBuf,

    /// Number of files to keep in the download directory. If exceeded, delete the oldest file, then
    /// create a new one.
    #[clap(short = 'n', default_value = "30")]
    max_files: usize,

    /// Timeframe for each downloaded file. Ex: An argument of 5m would mean that a new file gets
    /// generated every 5 minutes. The supported time units are `d` for days, `h` for hours, `m`
    /// for minutes, and `s` for seconds.
    #[clap(short = 'i', default_value = "1h")]
    interval: String,

    /// Host to pubsub service, work the same as `to` option of `publish` subcommand with the
    /// advantage that it will have publish and download using the same underlying stream.
    /// The publishing will stop when all files have been downloaded. The channel is always
    /// `shodan.banners`.
    #[clap(short = 'p', long)]
    publish: Option<String>,
}

fn time_str_to_duration(interval: &str) -> time::Duration {
    // TODO: error handling
    let (value, unit) = interval.split_at(interval.len() - 1);
    let value = match value.parse::<f64>() {
        Ok(value) => value,
        Err(err) => panic!("{}", err),
    };
    let secs_multiplier = match unit {
        "s" => 1,
        "m" => 60,
        "h" => 60 * 60,
        "d" => 60 * 60 * 24,
        _ => panic!("Invalid time unit, allow second, minute, hour, and day"),
    };
    time::Duration::from_secs(secs_multiplier).mul_f64(value)
}

pub async fn handler(mut client: FirehoseClient, ctx: Context, args: Args) {
    let mut path = args.directory;
    if path.exists() {
        println!("Download directory {} exists..", path.display());
    } else {
        let _ = fs::create_dir(&path).await;
    }

    let mut pubsub_service = None;
    if let Some(host) = args.publish {
        pubsub_service = Some(PubsubService::from_host(&host).await);
    }
    client = client.full().chunks(ctx.batch_size);
    if ctx.lines {
        client = client.lines();
    }
    let mut stream = client.into_stream().await;
    let mut interval = time::interval(time_str_to_duration(&args.interval));
    let _ = interval.tick().await; // immediately return
    let mut current_index = 0;
    let mut file_queue: Vec<String> = Vec::with_capacity(args.max_files);
    loop {
        let filename = Local::now().to_rfc3339_opts(SecondsFormat::Secs, false);
        path.push(filename);
        let file_path = path.display().to_string();
        if current_index >= args.max_files {
            let file_index = current_index % args.max_files;
            let file_entry = file_queue.get_mut(file_index).unwrap();

            // Delete the oldest file, ignore on any error, then replace with a new file
            let _ = fs::remove_file(&file_entry).await;
            *file_entry = file_path;
        } else {
            file_queue.push(file_path);
        }

        let file = File::create(&path).await.unwrap();
        let mut buffer = BufWriter::new(file);
        println!("Downloaded file at {} ({})", path.display(), current_index);
        loop {
            tokio::select! {
                _ = interval.tick() => break,
                banners = stream.next() => {
                    if banners.is_none() {
                        break;
                    }
                    let banners = banners.unwrap();
                    // TODO: Remove the if
                    if let Some(publisher) = pubsub_service.as_ref() {
                        if let Err(_) = publisher.publish("shodan.banners", &banners).await {
                            println!("Something went wrong with banners publishing!");
                        }
                    }
                    let _ = buffer.write_all(&banners).await;
                }
            }
        }
        buffer.flush().await.unwrap();
        path.pop();
        current_index += 1;
    }
}

#[cfg(test)]
mod tests {
    use time::Duration;

    use super::*;

    #[test]
    fn test_convert_time_str_to_duration() {
        assert_eq!(time_str_to_duration("5s"), Duration::new(5, 0));
        assert_eq!(time_str_to_duration("0.5h"), Duration::new(1800, 0));
        assert_eq!(time_str_to_duration("0.1m"), Duration::new(6, 0));
        assert_eq!(time_str_to_duration("0.01s"), Duration::new(0, 10000000));
    }
}
