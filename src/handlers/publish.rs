use anyhow::Result;
use rdkafka::producer::{FutureProducer, FutureRecord};
use rdkafka::ClientConfig;
use redis::aio::ConnectionManager;
use tokio_stream::StreamExt;
use url::Url;

use crate::client::FirehoseClient;
use crate::Context;

/// Republish data from Firehose to queue services
#[derive(Clone, Debug, clap::Args)]
pub struct Args {
    /// Host to pubsub service. The cli will try to parse the URL and then decide which service it
    /// will publish to. Detect if the host is a valid Redis URL first, then fallback to Kafka,
    /// otherwise panicked.
    #[clap(short)]
    to: String,

    /// Channel/topic name, depends on which service you are using.
    #[clap(long = "channel", default_value = "shodan.banners")]
    channel: String,
}

async fn init_redis_connection(host: &str) -> ConnectionManager {
    redis::Client::open(host)
        .expect("Failed to parse redis URL")
        .get_tokio_connection_manager()
        .await
        .expect("Failed to make Redis connection")
}

pub fn init_kafka_producer(hosts: &str) -> FutureProducer {
    ClientConfig::new()
        .set("bootstrap.servers", hosts)
        .set("message.timeout.ms", "5000")
        .set("allow.auto.create.topics", "true")
        .create()
        .unwrap()
}

pub struct PubsubService {
    inner: std::boxed::Box<dyn Publisher>,
}

impl PubsubService {
    pub async fn publish(&self, channel: &str, banners: &[u8]) -> Result<()> {
        self.inner.publish(channel, banners).await
    }
}

#[async_trait::async_trait]
pub trait Publisher {
    // TODO: Hanlde response
    async fn publish(&self, channel: &str, message: &[u8]) -> Result<()>;
}

#[async_trait::async_trait]
impl Publisher for redis::aio::ConnectionManager {
    async fn publish(&self, channel: &str, message: &[u8]) -> Result<()> {
        let _ = redis::Cmd::publish(channel, message)
            .query_async::<_, u32>(&mut self.clone())
            .await?;
        Ok(())
    }
}

#[async_trait::async_trait]
impl Publisher for FutureProducer {
    async fn publish(&self, channel: &str, message: &[u8]) -> Result<()> {
        let record: FutureRecord<String, _> = FutureRecord::to(channel).payload(message);
        match self.send(record, std::time::Duration::from_secs(0)).await {
            Ok((_partition, _offset)) => Ok(()),
            Err((err, message)) => {
                println!("kafka error: {:?} {:?}", err, message);
                Err(err.into())
            }
        }
    }
}

impl PubsubService {
    pub async fn from_host(host: &str) -> Self {
        let url = Url::parse(host).expect("Invalid service URL");
        match url.scheme() {
            "redis" | "redis+ssl" | "rediss" | "redis+unix" | "unix" => {
                let client = init_redis_connection(host).await;
                Self {
                    inner: Box::new(client),
                }
            }
            _ => {
                // Publish to Kafka if Redis is not specified
                let client = init_kafka_producer(host);
                Self {
                    inner: Box::new(client),
                }
            }
        }
    }
}

pub async fn handler(mut client: FirehoseClient, ctx: Context, args: Args) {
    let pubsub_service = PubsubService::from_host(&args.to).await;
    client = client.full().chunks(ctx.batch_size);
    if ctx.lines {
        client = client.lines();
    }
    let mut stream = client.into_stream().await;
    while let Some(banners) = stream.next().await {
        if let Err(_) = pubsub_service.publish(&args.channel, &banners).await {
            panic!("Something went wrong!");
        }
    }
}
